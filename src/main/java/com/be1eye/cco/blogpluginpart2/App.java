package com.be1eye.cco.blogpluginpart2;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sap.scco.ap.plugin.BasePlugin;
import com.sap.scco.ap.plugin.annotation.PluginAt;
import com.sap.scco.ap.plugin.annotation.PluginAt.POSITION;
import com.sap.scco.ap.pos.dao.IReceiptManager;
import com.sap.scco.ap.pos.entity.ReceiptEntity;
import com.sap.scco.ap.pos.entity.SalesItemEntity;
import com.sap.scco.env.UIEventDispatcher;
import com.sap.scco.util.logging.Logger;

/**
 * Hello world!
 *
 */
public class App extends BasePlugin {
	
	private static final Logger logger = Logger.getLogger(App.class);

	@Override
	public String getId() {

		return "blogpluginparttwo";
	}

	@Override
	public String getName() {

		return "CCO Plugin Part 2";
	}

	@Override
	public String getVersion() {

		return getClass().getPackage().getImplementationVersion();
	}

	@Override
	public boolean persistPropertiesToDB() {
		return true;
	}
	
	@Override
	public Map<String, String> getPluginPropertyConfig() {
		
		Map<String, String> propertyConfig = new HashMap<String, String>();
		
		propertyConfig.put("LIST_OF_MATERIALS", "String");
		propertyConfig.put("MESSAGE_FOR_CASHIER", "String");

		return propertyConfig;
	}
	
	@PluginAt(pluginClass=IReceiptManager.class, method="addSalesItems", where=POSITION.AFTER)
	public void checkSalesItem(Object proxy, Object[] args, Object ret, StackTraceElement caller) {
	
		if(args.length == 2) {
			ReceiptEntity receipt = (ReceiptEntity) args[0];
			SalesItemEntity salesItem = (SalesItemEntity) args[1];
			String[] materialIds = this.getProperty("LIST_OF_MATERIALS", "").split(",");
			
			List<String> listOfMaterialIds = Arrays.asList(materialIds);
			
			if(listOfMaterialIds.contains(salesItem.getMaterial().getExternalID())) {
				showMessageToUi(this.getProperty("MESSAGE_FOR_CASHIER", ""), "info");
				logger.info("Asked for item " + salesItem.getMaterial().getExternalID() + " in receipt " + receipt.getId());
			}
			
		}
	
	}
	
	private void showMessageToUi(String msg, String type)
	{
		Map<String, String> dialogOptions = new HashMap<String, String>();
		dialogOptions.put("message",msg);
		dialogOptions.put("id", App.class.getSimpleName());
		dialogOptions.put("type",type);
		dialogOptions.put("maxLifeTime","30");
		UIEventDispatcher.INSTANCE.dispatchAction("SHOW_MESSAGE_DIALOG", null,
				dialogOptions);
	}

}
